# hydron

*Have any amount of [Proton](https://github.com/ValveSoftware/Proton)s!*

This is a wrapper script for Valve's Proton tool to allow different versions to be used depending on the game being run.

Because a dropdown per game is asking too much apparently...

## Dependencies

* `zsh`
* (optional but recommended:) `/usr/bin/zenity` (because the version in the steam runtime sucks)
* coreutils:
    - `cut`
    - `grep`
    - `find`
    - `basename`
    - `dirname`
    - `readlink`
    - `touch`
    - `tee`
    - `tail`
    - `cat`
    - `rm`
* not technically, but logically I guess: `Steam` and `Proton`

## Installation

* if you don't have a `~/.steam/steam/compatibilitytools.d/` folder, create it.
* put the hydron folder into `compatibilitytools.d` as is.

## Usage

The version selection logic works as follows:

* if `$HYDRON_ISOTOPE` is set in the environment, it is used
* if a version is specified in the file `isotopes`, it is used
* if the user can be asked by means of a dialog box, the answer is appended to `isotopes` and used
* otherwise (=if there is no dialog box tool installed or the user cancels it) it just fails

the value specified in `$HYDRON_ISOTOPE` and/or `isotopes` is the folder name inside `compatibilitytools.d` (or `steamapps/common` as a fallback for vanilla).

the `isotopes` file has the following syntax (and will be created if it doesn't exist):

    # this is a comment
    # useful for keeping track what the hell those steam IDs were

    # empty lines are ignored
    # lines are \n terminated (get outta here, DOS!)

    # entries are in the syntax "gameid $HYDRON_ISOTOPE":
    # usually gameid means the Steam AppID here, but it's technically the basename of the game's $STEAM_COMPAT_DATA_PATH
    359320 Proton 3.16-6 Beta ED
    238960 kakra-wine-proton_3.16-4.0-rc5-unofficial-1
    517630 proton-localbuild

so with that file (on my system where those folders exist), running:

* Elite Dangerous (appid 359320) would automatically use the [Proton 3.16-6 Beta ED](https://github.com/redmcg/wine/releases) version
* Path of Exile (238960) uses [kakra's Patch](https://github.com/kakra/wine-proton/releases)
* and Just Cause 4 (517630) just uses the normal Proton 3.16 I built locally.
* anything else it would ask me and save the choice.


**NOTE**: to use a "vanilla" version you can put it into your `compatibilitytools.d` manually since steam has a habit of uninstalling Proton versions it thinks could be "unused"...
see the [docs](https://github.com/ValveSoftware/Proton#build-proton), but tl;dr: `make install` is your friend.

According to [@kakra](https://github.com/ValveSoftware/Proton/issues/58#issuecomment-453101647) it is possible to install any vanilla version in the "Tools" tab of your steam library, which my script does try but I can't guarantee any success in case steam decides to mess with that folder.